package zio.keeper

final case class MetadataID(value: String) extends AnyVal
